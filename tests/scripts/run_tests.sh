#!/bin/bash

#
# Begin wih a very simple, "does the file exist?" check...
#
if [ ! -f /usr/lib/firmware/regulatory.db ]
then
	exit 1 # no regulatory database installed, so fail
fi

exit 0 # got here? indicate success...
